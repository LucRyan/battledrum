------------------------------------------------------------
--------------------- Introduction -------------------------
------------------------------------------------------------
Click "BattleDrum" to execute the game. (Version 1.4.5)

The Game Could run under Windows OS.

- StoryLine
In 201 A.D. ancient China, The two groups that dominated 
the court during the second half of Later or Eastern Han 
were the consort clans and the eunuchs. After Emperor Huan 
had ended the power of the consort clans, he and his 
successor Ling were only supported by the eunuchs. The 
attempt to extinguish the powerful eunuch faction in 189 AD 
was successful, and warlords that took over the regency for 
the weak emperors filled the power vacuum. The ruthless 
general Dong Zhuo who had sacked the capital Luoyang, the 
player is assigned as a commander of a troop that is in this 
endless war. You must encourage your troop and lead them to 
victory, which you have are the “Art of War” and Drum beside 
your hand.

- Control
Press "ESC" to pause game.
The rhythm beats has binded to your Keyboard.
1.Pa  --> "A".
2.Pon --> "D".
3.Don --> "S".
4.Ta  --> "W".

Rhythm set to 76 bpm and if you beat a wrong rhythm, you
could wait 2 seconds or press "SPACE" to cancel the command.

1.Pa,Pa,Pa,Pon: Move Forward.
2.Pon,Pon,Pa,Pon: Attack.
3.Pon,Pon,Pa,Pa: Retreat. 

And after two minutes while playing you'll find a new special 
command in BattleDrum.

- Game Objective
You need to defend the endless enemy troops and try to get
your highest score, if you let your basement health below
zero you'll lose the game.

- Difficulty Progression
Gameplay progression is based upon specific timed levels. 
Each level will be progressively more difficult than the 
previous one.

------------------------------------------------------------
------------------ Development Progress --------------------
------------------------------------------------------------
For BattleDrum Ver0.1.3, the troop just can free walking on 
the battle field.

Create the Knight.
- Press "wasd" to control.
- Press "space" boost your soldier.
- Press "j" to attack.
- Press "k" to defense.


For BattleDrum Ver0.5.3, the Knight and Infantry could move
together.
- You could press four times "w" to move your army up.
- You could press four times "s" to move your army down.
- You could press four times "a" to move your army left.
- You could press four times "d" to move your army right.


For BattleDrum Ver0.6.5, I've added the rhythm system(the 
beta edition).
- The sun could feel your rhythm.
- Add background music.
- Add test drum sound.
- Add rhythm bar to help you beat the drum.


For BattleDrum Ver1.0.3, I've added the completed rhythm 
system and beta enemy AI system.
- Archer could use their bows.
- Knight and Infantry could attack in a series action.
- Player has THREE combination to control the troop,
   
    1.Pa,Pa,Pa,Pon: Move Forward.
    2.Pon,Pon,Pa,Pon: Attack.
    3.Pon,Pon,Pa,Pa: Retreat. 

- Add score board and best score board.
- Add drum visual effect Pon, Pa, Ta, Don.
- Add drum audio: djembe7,8,9,10.
- Add time control system.
- Add completed rhythm system, rhythm set to 60 bpm.
- Enemy want to destory player's basement.
- There has a Mystery Enemy.


For BattleDrum Ver1.4.5, I've added the Menu System, 
Enemy System and Skill direction page.
- Start menu has Play and Direction button.
- The enemy has a basement and they'll start march from it.
- Shield has a Special Skill.

    1.TA,Ta,Don,Pon: Special Skill.

- Add the basement health and game timescale system.
- Add the enemy attack system.
- Refine the enemy AI system, they has some new features.
- Add Pause Game function, player could see direction.
- Add three kinds and 6 diffierent enemies'
 AI.


Yang WANG
Nov 27 2011
